# CNC Projects #

### [CNC Threading](#cnc_thread) ###

![Figure 1 - Single point threading.](./img/single_pt.png)  

* Developed an understanding of UN thread calculations and writing cutting programs on a HAAS lathe.  
* Used G76 for external threads, with G84 for tapping. 
* [G-Code](./src/single_pt.txt)

Material|Blank Size
--------|----------
6061|1.5 DIA x 1-7/8
 
Slot #   | Tool
:------: | ----
1 | Carbide insert 30-deg OD - .015 TNR
2 | .118 grooving tool
3 | .375 DIA spot drill
4 | #1 135-deg SP drill
5 | 1/4-20 UN roll tap
6 | 60-deg threading insert

### [HAAS CNC Lathe Roughing and Finishing](#haas_thread) ###

![Figure 2 - Roughing, finishing and drilling.](./img/rough_fin.png)

* Introduction to lathe rough / finish passes (G71 / G70) with type 1 and type 2 moves.  
* Required calculating part geometry in absolute and incremental coordinates.  
* Included round cuts using G02 / G03 commands.
* [G-Code](./src/rough_fin.txt)

Material|Blank Size
--------|----------
6061|1.5 DIA x 2-1/2
 
Slot #   | Tool
:------: | ----
1 | Carbide insert 30-deg OD - .015 TNR
